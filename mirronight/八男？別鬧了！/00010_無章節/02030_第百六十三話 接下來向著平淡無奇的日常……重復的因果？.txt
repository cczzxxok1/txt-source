第百六十三話 接下來向著平淡無奇的日常……重複的因果？


「鮑邁斯特邊境伯，在嗎？」

「是？啊啊，導師啊」

「又在做土木工程？」

「是羅德里希拜託我的哦。雖然已經是家常便飯了……」

那個事件發生一周後，導師訪問了正在鮑邁斯堡郊外用魔法做土木工程的我。

他似乎是來替王宮傳信的，這個任務最適合的人選就是他了吧。

中央的貴族和王族們正忙著處理普拉特伯爵家和嗎魔道具工會，所以就輪到很閑的導師出場了。

「工程已經結束了，要一起回我家去嗎？」

「可以的話希望能有什麼東西吃！」

「埃莉絲應該有準備」

「真是太好了！」

做完預定工程的我和導師一起用『飛翔』飛回了家裡，埃莉絲她們果然正在聽庭院中的桌子上準備午餐。

「啊，是導師」

「看上去很美味的飯菜啊！露易絲姑娘」

「因為有埃莉絲負責指揮哦」

「露易絲小姐，聊天之前請先讓手動起來」

「好——的」

「露易絲，你這樣對小孩子的教育不好哦」

「嗚嗚……伊娜醬已經完全變成熱心教育的媽媽了呢」

露易絲、卡特莉娜、伊娜都在幫忙準備午餐。

「人數這麼多可真辛苦」

「沒錯，咱們家不論大人還是孩子都很多呢」

泰蕾莎和艾茉莉嫂子也正在往桌子上擺盤碗，並忙著盛飯盛菜。

「對，要把魔力流收細。然後要盡可能多弄幾條魔力流出來」

「大家基本上都已經很擅長控制魔力了嘛」

午飯還要等一會，所以維爾瑪和卡琪婭跑去指導孩子們練習魔法。

「還不行呢」

「感覺還是贏不過麗莎啊」

「有嗎？」

「露露的魔力還會成長，所以還是有可能超過麗莎的吧。俺這個秋津洲人的魔力就太少了。真是羨慕還在成長的露露」

「等成了可以獨當一面的魔法使，我要把海竜從南海徹底驅逐讓大家可以隨便出海或捕魚，所以必須趁現在變得更強才行」

麗莎正在教藤子和露露怎麼集中魔力。

「啊，是導師。歡迎」

「艾爾文……你在做什麼？」

「只是幫把手而已」

看到也在幫忙準備午餐的艾爾，導師吃驚的瞪圓了眼睛。

按照琳蓋亞大陸的常識，這種光景本來不可能在冒險者的野營地之外的場所出現才對。

「導師大人也請說說他吧。太讓我難堪了」

遙向導師提出讓他勸艾爾快住手的請求。

按照現在琳蓋亞大陸的常識，男性幫忙做飯這種行為，只會讓人產生『女性在什麼地方偷懶吧！』的想法。

雖然有野營中的冒險者這唯一的例外，但如果艾爾在家裡這麼幹，就只會害遙很丟面子而已。

其實這方面我也是一樣。

「魔族的男性都會做這樣的事哦。所以就像威爾說的那樣，男人也在幫手的範疇內做些家事或育兒行為也挺好的嘛」

「你受了誰的影響麼？」

「這個嘛，昨天莫爾他們來了……」

昨天，包括做協商交易的工作在內，莫爾他們帶了妻子和孩子來這邊玩。

他們都已經成了愛麗經營會社的幹部候補，並和在公司內相識的女性結婚並生了孩子。

這適應力真是相當出色，連他們的老師恩斯特都嚇了一跳。

『那麼，考古學方面呢？』

『沒時間搞……』

『老師，考古學又不能當飯吃』

『有時間的時候來給您幫個忙就足夠了吧？』

『我的學生們都變得很忙碌了啊』

『工作嘛』

『雖然有休假，但那種時間要優先用在家人身上』

『老師，結婚之後可是很辛苦的哦』

『咔啊——，你們就不懂得孝敬恩師嗎』

話雖如此，其實現在恩斯特正以鮑邁斯特邊境伯家食客考古學者的身份，與王國學院派遣的學者學生們聯手對國內的地下遺跡進行尋找發覺。

就是說，王國政府判斷只要給恩斯特足夠的誘餌（遺跡和考古學調查），他就會老老實實的。

恩斯特雖然那個樣子但其實很照顧自己學生，現在他也教了人類新學生們各種東西。

因為這些原因，他現在只偶爾會來鮑邁斯特邊境伯領。

昨天他大概是專門來見學生和他們的家人吧。

「恩斯特也會來啊……」

「導師很不擅長應付恩斯特呢」

「沒有那種事。艾爾文」

導師算是體育系的人物，恩斯特則完全是文科系的人物。

雖然這兩類人的相性是很糟糕，但其實他們在自己喜歡的事物最優先這類特點上有很多相似的地方。

兩人相性這麼差，說不定其中也包含了同類相斥的理由。

另外現在魔力量幾乎相同這點，可能也讓他們彼此開始在意對方吧。

「說到底，就是因為與那個男人做容量配合，在下的魔力才只長到和那傢伙相同的程度！」

「您這說法挑釁的味道太重了吧……」

「我也這麼覺得」

導師的說法完全就是找茬。

畢竟即便導師和愛麗做容量配合，他的魔力也只能增長到和恩斯特相同的程度。

「說起來。莫爾他們真的會幫忙做家事哦」

莫爾他們選擇了夫妻都出去工作的生活方式。

他們的妻子們產後也都回歸了職場。

所以，他們三個也都有幫老婆做飯的習慣。

「琳蓋亞大陸最終也加入這個潮流的可能性很高吧。所以我們走在先端也沒什麼不好的。對吧，威爾？」

「那倒是沒錯，但是艾爾，你絕對不要親手做料理」

男性要不要幫忙做家事，我覺得那完全是個人和夫婦之間的問題。

我沒打算連這種事都干涉，但艾爾每次做料理總是做出一堆難吃到死的東西，所以只有親手做料理這事我希望他千萬不要。

「誒誒———，最近一直參考遙的料理，我也可以……」

「不行」

與和食很相似的瑞穗料理在各種方面都非常纖細。

我覺得這不是味覺容忍範圍大過頭的艾爾能做的東西。

「食材被浪費太可惜了所以不行」

「威爾好過分……」

我一點都不過分。

只是不想吃失敗的料理而已。

而且，老媽們不是也常說嗎？

不可以浪費食物。

「威德林，過得還好嗎？」

「鮑邁斯特邊境伯殿下，我們來打擾了」

「陛下看來真的能使用幻之魔法『瞬間移動』了呢」

愛麗、萊拉小姐、盧米三人突然出現在了我的視野中。

在奧托事件中和我做了容量配合的愛麗，在那之後不久魔力就停止了提升，和我不同她幾乎沒怎麼享受到容量配合帶來的恩惠。

不過，她學會了現在的魔族中極少有人會用的『瞬間移動』，現在似乎可以自由的在各地之間移動。

今天也是，在秋津洲島、鮑邁斯特邊境伯領西部視察完畢後，她就出現在了這裡。

「做過容量配合和可以使用的魔法種類增加這種事到底是經常發生，還是僅限魔力增加後才會出現呢，真是不可思議啊」

「原理雖然不明，但這個魔法真方便。對我們會社的擴大、削減交通經費方面貢獻多多」

「陛下，請您好好期待收益率的改善吧。我們自己能使用『瞬間移動』就是這麼的方便」

「一瞬間就能抵達取材地點。真方便啊」

「是記者嗎。就算你來取材，這裡也只有一場午宴而已」

「導師先生，我就是來採訪鮑邁斯特邊境伯與家族其樂融融的場面，然後拿回去寫成博取輿論好感的報道啊。這是之前大新聞的謝禮呢」

盧米因為之前獨佔了奧托一伙與奧斯瓦爾德暗殺未遂事件的詳細內幕，而到出版了一條獨家大新聞。

「會社上層那些傢伙也只能露出難看到不行的臉色而已呢。他們已經察覺到不能對我進行處罰的自己在這件事上輸了」

現任議員為王國貴族和魔道具工會做暗殺中介，還收取高額的中介費。

目前，作為執政黨的民權黨正因為這件事遭受輿論和在野黨的同時攻擊，已經成了基本無法發揮政府機能的狀態。

任期到期前議會就解散的展開已經無法阻止，甚至報紙上都開始推測他們在下次選舉會不會慘敗了。

硬賴在位子上拖到任期到期只會招來更多的批判進一步加大輸掉選舉的可能性，所以現在那群人似乎正在討論到底選個什麼時候解散議會。

「奧斯瓦爾德議員呢？」

「以挪用政治資金和逃稅的罪名被起訴了，但我覺得他不會接受教訓。至於幫忙中介暗殺鮑邁斯特邊境伯這件事，因為他並不是實行犯事件又發生在他國領地，所以無法以此起訴他」

「我就猜到會這樣」

現在能知道的，也就是魔族之國的談判團在談判席上因為這件事遭到了王國的責難。

因為交涉陷入不利，那些人哪怕硬頂也不會以暗殺未遂嫌疑的罪名逮捕奧斯瓦爾德議員吧。

「至於那些實行犯，也被認為這麼一直別回佐奴塔克共和國比較好呢」

即便讓他們回去，也不會有人嘗試以暗殺我的罪名逮捕他們。

因為事件並不是發生在佐奴塔克共和國國內，所以無法走法律程序的逮捕他們。

然而，如果就這麼無視這群人的話又會被新聞曝光。

『雖說發生在他國，但暗殺大貴族的人最後居然被視為無罪，這是法治國家該做的事嗎？』肯定會出現這樣的批判吧。

「民權黨的議員有很多是律師出身嘛。如果對法律條款擴大解釋來逮捕那些，類似奧托那樣的團體就會罵他們是賣國奴。可不逮捕的話，那些民權黨的支持者又會批判他們侵害人權」

身為首領的奧托雖然已經死了，但另外九名參與暗殺的暗殺者以及他們乘坐的魔導飛行船的船員現在仍被收監在監獄裡。

他們到現在仍被魔力吸收繩索拘束著，沒辦法簡單逃走。

「這是蜥蜴自己斷尾的行為！」

「導師先生的這個說法沒法反駁呢。總之，我國政治現在陷入了大混亂。民權黨政權也成了風中殘燭……」

「按照陛下傳來的情報，現在交涉團正在和國權黨的政治家們談判。陛下說，那些人雖然難對付但至少會認真進行交涉」

王國已經猜想到魔族之國的議會很快就會解散，而且大部分人都認為接下來的選舉民權黨也只會慘敗

既然如此，盡早和下一批執掌政權的人開始對話比較好。

「交涉團團長雷米女士以議會解散和選舉將近為理由擅自離開迪拉巴雷斯群島這件事，也遭到了批判呢」

如果不能通過選舉，政治家就無法稱為政治家。

然而即便如此，翹掉團長這樣重要的交涉工作回去準備選舉這種行為，似乎還是遭到了魔族們的嚴厲批判。

「從樹上跌下來的猴子仍舊是猴子，可在選舉中落選的政治家就只是個普通人了」

「噢噢！鮑邁斯特邊境伯先生！您的這句話說得真好。記錄記錄……」

盧米把我的發言記錄到筆記本上。

其實這句話是我在反對奉勞動工會的命令去為他們做選舉應援時，從一位年長的上司那裡聽來的。

所謂政治家就是用漂亮話換取他人敬意的人，所以那些傢伙為了不在選舉中落選都很拼吧。

雖然我當時出於義務加入了勞動工會，可那個組織的會費不僅很貴還有被幹部們挪用拿去做豪華旅游經費的傳聞。而且我們明明還有工作要做卻得在假日裡被叫去為他們的選舉活動造勢，總之在我印象裡勞動工會從沒派上過什麼用場。

說起來，民權黨的支持者也是來自勞動工會。

看來還是別太期待那些人能有什麼作為吧。

「鮑邁斯特邊境伯先生，您對民主主義的理解真是非常透徹呢」

「有嗎？」

我前世是活在民主主義的世界裡沒錯，但被說對這東西非常熟悉就有點微妙。

「連國權黨的政治家都說，王國貴族比預想的更理解民主主義導致交涉進行的很吃力」

「陛下他，偶爾會叫鮑邁斯特邊境伯過去詢問一些事情！」

我只是被問了『你對魔族的政治形態有什麼看法？』後如實回話了而已啊。

我的話似乎非常有參考價值的樣子。

交涉，真的是種只要是能讓自己有利的東西就全都拿來用上的行為。

「我想再過一段時間，這次的事件就能告一段落了哦」

「我們也想盡早了結這次的事！」

對我進行暗殺未遂的事件，最後以魔道具工會解散成為獨立組織為結局迎來了終結。

和帝國相同他們將被整合為魔導工會的一個部門，反對這種做法的舊工會上層和老頭們都被趕去幹粗活的工作崗位。

相對的，年輕的魔道具工匠們都被指名擔當幹部，並被要求以協助魔導工會發展魔導技術為目標。

這其中要最優先完成的，就是有可能成為趕超魔族魔道具技術的王牌『魔法陣板』的量產化，以及刻入魔法陣模式的研究。

以日本來說，就是類似對半導體和控制系統的研發。

如果這研究能成功的話，人類就有可能製造出性能足以對抗魔族產品的魔道具。

不過用來刻上魔法陣的硅板和極限鋼板，暫時只有我能夠提供。

本來極限鋼就只有我能做出來，所以這東西當然只有我能造。

另外，因為這個世界的人不知道硅這種元素只把硅板視為陶瓷的亞種，所以硅板也只有我能提供的了。

「工作增加了真麻煩啊。導師」

「這是為了我們的國家。忍耐吧」

我說導師，你自己曾經忍耐過什麼嗎？

至少我覺得沒有。

「魔道具工會的那些老人，全都被降職到最底層專心製作魔道具去了！」

「沒有被開除呢」

「他們有魔力和技能，開除的話太可惜了！」

夏杰瓦德會長雖然死了，但會作為罪犯永遠留下不光彩的記錄。

原本他們一族的人即便不能使用魔法，也可以就任工會內的高薪職位，或是在和工會有關係的魔道具工房商會就職，但現在這些人已經全部確定會被炒魷魚。

這倒不是陛下強行提出要求，本來靠關係雇人這種行為，就有期待靠這個人的關係獲益的心態在裡面。

所以現在如果有人繼續雇傭成了犯罪的夏杰瓦德會長的親戚，就只對那個人的利益造成損失。

魔法使的資質無法遺傳，所以這一族中沒有其他人可以使用魔法，結果在這種時候他們就被從玫瑰色的人生中打入了社會最底層嗎。

反正他們都是靠夏杰瓦德會長的權勢才得到現在的地位的，那麼在夏杰瓦德會長倒臺時跟著失去一切就不值得同情吧。

「夏杰瓦德會長本人雖然死了，但其他前幹部就要為增加魔道具產量一直工作到死！陛下也說了，如果有人敢逃走就會落得和夏杰瓦德會長同樣的下場！」

那些人雖然因為年紀大了多少有些衰弱，但也並不是就沒法做魔道具。

魔道具工匠的薪水向來很高，所以他們也算和死掉的夏杰瓦德會長一起給魔道具工會造成了很大損失，這部分必須讓他們以繳納罰金的形式進行補償才行。

全體被開除成了尼特的夏杰瓦德會長族人也交給了呀，耳膜照顧，這麼一來那些人至少能過上普通庶民的生活吧。

這說不定是種很毒辣的懲罰。

「交涉方面，國權黨的議員們已經到了迪拉哈雷斯群島！那些人似乎有意把迪拉哈雷斯群島上的設施轉讓給霍爾米亞邊境伯，然後從那裡撤走！」

那麼偏遠地域的孤島群，連魔族也不會想要吧。

警備隊的駐留經費，付給基本派不上用場的青年軍屬人員工資，光是維持這些就足以造成財政大赤字。

所以對方似乎覺得還是趕快把這裡還給主張自己是這裡所有人的霍爾米亞邊境伯比較好。

交涉開始後島上是增健了很多建築物，那些東西似乎都會留下。

反正拆除的話又要多花一筆錢，所以乾脆當成非公開向霍爾米亞邊境伯道歉時用的賠禮嗎。

雖然也有『這種即便強調所有權，但迄今為止從沒派上用場的島上的建築物，對霍爾米亞邊境伯到底有什麼用』這個疑問就是了。

也有人提出了讓迪拉哈雷斯群島成為交易中轉地的方案，但果然還是被人以這地方太偏僻為由否決掉了

兩邊的人都覺得繼續用既有的港口進行交易更快捷，霍爾米亞邊境伯今後到底要怎麼使用魔族返還回來的迪拉哈雷斯群島現在還不明。

「反正也不是我家的領地」

「也不是王國直轄地。似乎有人建議把那裡當成漁業基地」

其實那些島只能以那種方式運用。

那些島上不適合進行農業，用水也很容易陷入不足，所以上面無法住太多人。

但要是在那裡設置能將海水轉化成淡水的魔道具的話，說不定就能成為一個最適合做漁船休息站的地方。

正好上面還留有現成的建築，所以這種做法應該用不了多少經費。

「老師，飯菜準備好了」

「導師大人也請來用餐吧」

「噢噢！不好意思啊」

「我們今天也幫了很大忙呢」

「貝蒂和兄長一樣很擅長料理」

「畢竟那是老哥唯一拿得出手的特技」

飯菜準備好後，阿格妮絲、辛迪、貝蒂來招呼我們吃飯。

這三人最後終究也成了我的妻子並為我生了孩子，現在已經十分習慣我家的生活。

今天她們也來幫埃莉絲她們準備午飯。

「夫君，今天的主菜是用秋津洲島近海海竜的肉做的肉料理哦」

「涼子、雪，海竜的數量現在減少了嗎？」

「秋津洲島附近確實減少了」

「其他海域就沒有了」

平常只是個裝飾的秋津洲島總代官涼子，以及作為實質上的總代官工作的雪，今天也被我叫來參加了這場午宴。

在她們兩人身邊，父親堪稱可疑人物代名詞的唯也在。

除了還沒結婚的菲莉涅外，我的妻子們今天全都到齊了。

「現在這種做法好嗎？」

「久秀殿下說了一切都交給他」

對導師的提問，雪這麼回答道。

按照秋津洲島前領主階級的那些人的說法，涼子、雪、唯三人應該盡可能的留在我身邊多多生孩子。

然後請把那些孩子嫁進我們家或是來給我家當入贅女婿，所以現在每周有一半時間她們三個都是在鮑邁斯堡的我家度過的。

這期間的政務，由唯的父親久秀代理執行。

唯生下的孩子有著足以成為上級魔法使的資質，作為松永家的繼承人完全無可挑剔，所以久秀歡天喜地的把代理執行政務的工作搶了過去。

不過似乎有『請讓唯生更多孩子，然後嫁到我家來』、『會讓那孩子做繼承人所以來我家入贅吧』之類的請求大量出現搞得他很辛苦就是了。

涼子和雪似乎也被拜託『除了公開活動外請盡量留在鮑邁斯堡生更多孩子吧』的樣子。

「子孫滿堂啊」

「導師不是也生了大量的孩子？」

「還沒到鮑邁斯特邊境伯的程度」

「就是說啊。像我家就只有一個女兒咧」

對獨生女異常疼愛的布蘭塔克先生也加入了我和導師的對話。

我的孩子現在數量已經多到前世無法想像級別，但這也是這個世界的規矩。

魔族之國也一樣，雖然和平成年代的日本很相似但常識還是屬於這個世界的東西。

我覺得自己已經完全變成這個世界的人了。

使用魔法不會有任何不自然感，也成了貴族並擁有了廣大的領地。

王族和貴族的條條框框雖然很麻煩，但說我現在每天都過得很充實也不為過。

唯一還擔心的，也就是那邊世界的一宮信吾的身體現在變成怎樣了吧？

還有就是被我以憑依形式奪走身體的真正威德林到底如何了？

雖然有疑問，但我沒有解決疑問的方法。

「威爾大人，我餓了」

「確實啊。那就入席開吃吧」

「呵呵呵，朕因為是客人所以要坐威德林旁邊的席位哦」

「愛麗大人也真是學不乖」

「露易絲小姐，愛麗大人是用交易為我們家帶來莫大利益的重要客戶。不過另一邊的座位要由我來坐」

「埃莉絲真是滴水不漏啊……。不過也難怪，埃莉絲是正妻嘛」

我在家主的上座席位坐下後，愛麗和埃莉絲在我左右兩邊的席位上坐下。

「那麼，讓我們為兩家的繁榮乾杯」

「「「「「「「「「乾杯！」」」」」」」」」

聽到我的祝酒詞，大家都舉起注入紅酒的杯子乾杯。

「話說回來，這片未開發地明明原本什麼也沒有，現在卻發展到了這個程度」

「無人之地就是這樣的」

導師和布蘭塔克先生，一邊喝著紅酒一邊聊起了現在也仍在繼續擴大規模的鮑邁斯堡的話題。

聽他們這麼一說是感覺很不可思議，不過這些都是自己造成的帶來不可思議感更強烈。

感覺都要開始深思我被召喚到這個世界來是否有什麼含義了。

多半，我接下來都會作為威德林而非一宮信吾生活，最後死去吧。

等我死後，鮑邁斯特邊境伯家會變成怎樣呢？

要是能平安延續很多代就好了，我一邊這麼想一邊喝幹了杯中的紅酒。

「威德林，你仔細聽好我要說的話」

「是」

「你知道我為什麼，要給作為鮑邁斯特邊境伯家八男出生的你，取了我們家偉大的初代大人的名字嗎？威德林」

「因為我擁有規格之外的魔力？」

「沒錯！你擁有匹敵初代大人魔力。也就是說是相當於復活的祖先那樣的存在」

父親的長篇大論又要開始了。

而且他每次講的內容還都差不多。

我差不多也開始覺聽膩了。

我是作為在赫爾姆特王國擁有廣大領地的大貴族——鮑邁斯特邊境伯家的八男出生的。

雖然母親艾茉莉是側室，但因為生下來就擁有魔法的才能，所以父親很疼愛我。

如果光是這樣還好，可不知從什麼時候起我成了鮑邁斯特邊境伯家的新希望，還被逼著一次又一次的聽內容總是相同的偉大祖先的軼事。

哥哥們……看來已經為了不被牽連逃走了。

明明是側室的孩子卻擁有更多的魔力，這樣的我沒有被欺負的原因中，大概也有哥哥們總把我當做抵擋父親長篇大論的犧牲品的理由在裡面吧。

其實我也很想逃走，但對方畢竟是父親所以沒辦法那麼干，結果只能繼續耐心聽他講古。

「從初代大人開始，我們一族就很不可思議的可以把魔法才能遺傳給子孫。而且我們家還多次引入魔族的血脈。據說初代大人甚至曾娶魔族之王為妻」

初代大人看來是個相當大膽的人。

他雖是貧困之極的騎士爵家出身，卻靠魔法的才能立下各種功績最後成了大貴族。

並且還和王家建立了血緣關係，現在鮑邁斯特家不僅是五大邊境伯之首，還擔任了新領土南貝斯迪尼亞大陸的總督和聯絡官，深得王家信賴。

我們一族可以把魔法才能遺傳給子孫，這種現象從初代大人起就是如此了。

靠著這個優勢，鮑邁斯特邊境伯家迄今為止已經出了幾十位王宮首席魔導師。

不過，初代大人曾經留下話，遺傳魔力這種現象似乎會隨著時間推移效果變得越來愈弱。

實際上，現在即便是過去時代的中級魔法使也被當做天才對待。

至於上級魔法使，已經被稱為幾十年才會出現一次的奇跡了。

大多數魔法使都是初級。

其他的貴族家，甚至根本沒法把魔法才能遺傳給子孫。

即便有鮑邁斯特邊境伯家出身的人嫁進來或入贅，數代人之後遺傳魔力這個現象就會消失。

也有使用和魔族聯姻這種秘技的貴族家，但人類能與魔族聯姻的幾乎數量太少了。

而且，到最後魔力遺傳只能持續數代這種現象依舊會發生。

魔族那邊似乎也很為魔力量減少的問題煩惱，『為什麼連他們都會這樣』，很多學者都對這個課題進行過研究，可結果原因還是不明所以。

魔族原本擁有最先進的魔導技術。

他們在開發和生產『收集空氣中漂浮的微量魔力儲存到魔晶石中，再以這種魔晶石作為高效率動力源』的魔道具方面，可說是拔群的存在。

『對於魔族所處的環境，就算沒有魔力也不會困擾，他們從本能理解了這點於是魔力資質就這麼衰退了』，有人提出過這樣的論點。

人類這邊也是類似情況——和初代大人的時代相比，現今的魔導技術已經有了壓倒性的進步。

現在的魔道具只需要很少的魔力就能運作，所以就算魔力變少也不會有誰因為為難。

結果人類的本能也覺得魔力不再是生存必要之物，於是大家的魔力量就這麼減少了，有學者這麼解釋道。

「但是！我們鮑邁斯特邊境伯家力量來自於魔法和魔力！而就在我們為現今的困境頭疼的時候威德林你出生了！陛下也好！大多數貴族也好！全都將你看成了他們的希望！」

「希望啊……。那我該做什麼？」

「其實不久之前，王家決定讓一位公主大人下嫁給你」

「這種事有什麼稀奇的嗎？」

雖然通常來說公主應該優先嫁給哥哥他們就是了……。

是在那麼做之前哥哥們就拒絕了嗎？

「要嫁過來的是第三公主伊娜大人，就是那位久違出現在王家的天才魔法使。她最適合與你結為夫妻了，陛下和埃里希他們都這麼說」

「誒———！是那傢伙嗎？」

「喂！你怎麼稱呼伊娜大人呢！」

父親，雖然你這麼說，但那傢伙可是總把我視為對手的很麻煩的一個人啊。

我是從懂事起就和她熟識了，但那只是她總向我挑起魔法比試我隨便應付，然後每次她都輸掉說著下次一定贏的臺詞後跑路而已啊。

「父親，您也體諒一下每次都和她比試魔法的我的心情吧」

如果讓對方受傷就會出大事，所以每次我都得小心翼翼的手下留情，很辛苦的啊。

「威德林，你還是太年輕完全不懂女人心啊。其實伊娜大人她非常喜歡你，可卻沒法坦率的說出來。所以才用向你挑戰的方法拉近和你的關係。這不是非常動人的行為嘛」

「是這麼回事嗎」

也就是那個嗎，小孩子會故意欺負自己喜歡的異性？

不過，我覺得全力把『火球』射過來的做法也太過了……

「雖然這些都是我的個人猜測」

「父親……」

原來不是事實啊！

「總而言之。你將成為沒有孩子的大伯父大人的養子，然後得到分家家主的地位。埃里希雖然是優秀的繼承人，但你今後也要好好支持他。至於赫爾姆特和保羅他們，已經決定要去其他貴族家做養子女婿」

雖說最近全體人類的平均魔力大幅減少，但鮑邁斯特邊境伯家出生的孩子仍有一多半擁有中級魔法使的資質。

因此，鮑邁斯特邊境伯家的人總是需要完成『盡量多生孩子』這個義務。

可以嫁出去的女兒自不必說，連兒子們因為能成為沒有男子嗣貴族家的養子也非常受歡迎。

父親目前已經有了十三個兒子和二十一個女兒。

可即便如此好像仍被王家說不夠，所以我的弟弟妹妹們大概還會繼續增加吧。

「總之就是這樣，你要伊娜大人迎娶為正室了」

「這事，您要怎麼對埃莉絲她們說明？」

鮑邁斯特邊境伯家的孩子都會很早就和複數婚約者定下婚約。

我的正妻，也已經定為和教會有著很深因緣的王都強力大貴族之一霍恩海姆伯爵家的女兒埃莉絲了。

說起來，初代大人的正妻好像也是霍恩海姆家出身，名字也叫埃莉絲。

連是治癒魔法使用者這部分都很像。

雖然我還另有其他複數的婚約者，但如果王家現在硬塞給我一位公主大人做正妻的話，肯定會引發不必要的混亂吧……

「和其他家族有關的事王家會想辦法解決。現在，有個更重要的工作要交給威德林你！」

「難不成是……」

「你來向埃莉絲她們說明」

「父親你去做啊！」

「不要，太可怕了」

父親……。

再怎麼說你也是鮑邁斯特邊境伯吧，居然會害怕幾個的未成年小姑娘……。

「總之！我是很忙的，剩下的事就交給威德林你了」

父親早早結束了這次談話，然後把我從他的辦公室裡趕了出去。

「艾爾，為什麼我必須得去向埃莉絲她們說明這種事不可呢？」

離開辦公室後，我向自己的隨從——重臣阿尼姆家的五男艾爾文說明了甩給自己的不講理現狀。

「當家大人很忙的，這種事威爾你就自己解決啦」

「艾爾你也來幫忙啊」

「公主又不是要嫁我」

艾爾是我從小就認識的死黨。

所以只有我們兩個獨處時，他對我說話的口氣會變得相當隨便。

「不過，就算是王家也不該硬插進來吧」

「就是說啊。而且，對方送來的還是那位伊娜大人」

「那個人，真的是個超級瘋丫頭咧……」

說著這番話期間我回到了自己的房間，我的婚約者們已經在這裡等著了。

「親愛的，我為你泡了茶哦」

「埃莉絲，我們還沒正式結婚呢，『親愛的』這種叫法不妥吧」

雖然還要過些時日我們才能成人結婚，但為了在正式結婚前加深彼此的關係，我和埃莉絲從十二歲開始就一起生活了。

埃莉絲明明從不懷疑自己會成為我的正室，可現在卻突然有個公主大人插到了她前面。

把這種事說出來也太辛苦了……話說，這不是父親的工作嗎？

「是伊娜大人的事？」

「埃莉絲你都知道了嗎」

「是的。從父親大人那裡來了聯絡」

埃莉絲似乎已經通過攜帶魔導通信機，從霍恩海姆伯爵那裡聽說了伊娜大人的事、

畢竟伯爵是教會的大人物，會知道這類情報也理所當然的。

再說埃莉絲可是霍恩海姆家的長女，讓她嫁給鮑邁斯特邊境伯家的八男本來就太屈尊。

「這也是沒辦法的事。畢竟如今王家很少生出優秀的魔法使」

「是這樣嗎？」

「是的，陛下的子嗣中擁有中級以上魔力的人，就只有伊娜大人而已」

雖說魔力的多少和王家的實力並不掛鉤，但果然人們都覺得優秀的魔法使還是越多越好吧。

就是說伊娜大人和我的孩子會擁有多少魔力這件事，現在備受他們的期待嗎。

「畢竟威爾的魔力到現在還在增長呢」

「還好啦」

我的另一名婚約者，鮑邁斯特邊境伯家分家、擔任魔斗流教頭職務的奧弗沃格家的二女露易絲也插話進來。

鮑邁斯特邊境伯家的孩子從記事起，就會按照初代大人制定的訓率課程學習魔法。

魔力增加的訓練也會同時進行，可我明明已經擁有遠超上級魔法使的魔力量了，卻依舊沒有碰到魔力增長的極限。

所以周圍的人才會對『我這麼成長下去說不定可以得到和初代大人一樣多的魔力』這件事充滿期待。

「真是麻煩的情況。那麼想進行魔法比試的話，讓她和卡特莉娜比不就好了嘛？」

「伊娜大人好像完全不把我放在眼裏……」

第三婚約者，說是鮑邁斯特邊境伯家的附庸實質上就是分家的維爾肯子爵家的長女卡特莉娜，也是以婚約者的身份從小就和我一起生活的。

如果伊娜和在我看來與她勢均力敵的，成人前就已經被稱作天才的卡特莉娜互相競爭的話，她的魔法使實力應該也能多少提升一些才對。

明明是這樣，可從過去開始伊娜大人就很執著的只向我挑起比試，然後還每次都輸。

「威德林先生，可以每天都進行魔法比試了呢」

「我才不要啊！」

那樣太浪費時間了，我當然不願意。

「也對。說到底，威德林先生的婚約者只有我們這種印象已經固定下來了呀」

「說不定和名字也有關係。我和初代大人的妻子之一一樣都叫維爾瑪。大家也全是這樣」

「因為名字相同，所以和老公結婚後生下的孩子就都會是上級魔法使了？沒可能會這樣吧」

同樣是鮑邁斯特家分家阿斯卡邦家的二女兒魔銃使維爾瑪，以及說是附庸實質上就是分家的尤倫堡男爵家的長女卡琪婭也加入對話。

這兩人也是我的婚約者。

說到名字相同，初代大人的公開、非公開妻子據說有三十到五十名以上。

據說他的孩子數量有八十到一百四十人以上，這些孩子中有些女孩繼承了初代大人妻子的名字，她們都被指定要成為名字和初代大人一樣的我的婚約者。

大人們是覺得如果名字和初代大人夫婦相同的話，說不定就可以生出上級魔法使了嗎，沒想到魔力低落的問題居然嚴重到了能讓他們產生這種荒唐想法的地步。

王國上層的人，肯定現在全都抱著只要是根救命稻草就會死死抓住不放的心態吧。

「雖然是玩笑一樣的做法，但妾身也是因為有這個名字才能預定從菲利普公爵家嫁過來吶。帝國那邊為了確保更優秀的魔法使也很下力氣的哦」

我的婚約者中也有帝國出身的女孩。

她是菲利普公爵家的三女兒泰蕾莎，是個郎族出身以褐色肌膚為特徵的肉感美少女。

雖說從數百年前起王國貴族和帝國貴族聯姻就不再是什麼稀奇的情況了，但菲利普公爵家可是選帝侯家，看來帝國也對我抱有相當的期待。

「話說回來，即便是公主，在這種情況下硬插進來也真是大膽呢」

「沒錯誒……」

同樣是分家出身的麗莎比我大五歲。

她也是魔法使，也是因為『擁有和初代大人的妻子相同名字』這個理由成為我的婚約者的。

「雖然對方是公主大人，但還是身為丈夫的威爾比較厲害吧?那麼如果對方有什麼不滿意的話，用比試來降服她就好啦」

「艾爾，你能別說的那麼輕鬆嗎」

「這又不是在聊我的婚約者」

艾爾，你今天有點讓我火大。

「那麼，那位公主大人到底什麼時候過來？」

其實連艾爾，都經歷過好幾次伊娜公主向我挑起魔法比試的場面。

所以他也算和對方認識。

「每次給那個公主大人做護衛她都會到處亂跑瘋玩，真是麻煩死了。我總得和其他人事先反復商討才行」

本來，只是個八男的我帶艾爾一名護衛就足夠了，可現在因為有這份可說是返祖現象的魔力，弄得連魔族都很關注我。

於是父親強化了我的護衛陣容。

好像是因為萬一我被人綁架所有人都會很頭疼的緣故。

而我的婚約者們當然也在他們的保護範圍之內，所以艾爾才會覺得為作為瘋丫頭很有名的伊娜公祖做護衛很辛苦，還大大嘆了口氣。

「威爾，你沒從當家大人那裡聽到什麼有關消息嗎？」

「父親什麼也沒說過，所以他大概也不知道吧？埃莉絲知道麼？」

「再怎麼說，對方來這邊的具體時間這種程度的情報我還是得不到的」

「因為是公主大人，所以肯定正忙著進行各種準備吧？感覺她會帶很多隨從來嘛」

就像露易絲說的那樣，對方『姑且』也是公主大人。

作為王族，她應該正在進行和她身份相符的各種前期準備才對。

「就是這麼回事，她暫時還不回來」

「很遺憾！我已經來了！」

「出現了！」

「幹嘛啦！把人家說的好像幽靈一樣！」

本來還以為她不會說到就到的，沒想到伊娜公主已經到鮑邁斯堡的。

和過去每次見面時一樣，伊娜公主沒有穿禮服而是一身很適合去狩獵的打扮。

雖然她的裝備所用的素材與王族這個身份很相符，但這怎麼看都不是和婚約者打招呼時該做的打扮。

「讓我的好對手威德林做婚約者，看來爸爸也對王族中連中級魔法使都生不出來這件事感到焦慮了呢」

「還好對手咧……你不是一次也沒贏過威爾嗎」

「嗚！下次就能贏了啦！」

被露易絲指摘出的事實讓伊娜公主滿臉不高興。

「伊娜大人，您是來向威德林大人挑起魔法比試的嗎？」

「當然的吧」

「那我就放心了。也就是說，您並沒有和威德林大人結婚的意思吧。那麼，就按照當初約定的那樣，威德林大人的正妻由我來擔當了」

埃莉絲雖然平時溫和又溫柔，但這並不能改變她是歷史悠久的大貴族家的千金這個事實。

現在她就堂堂正正先一步對伊娜公主進行了牽制。

「為什麼會變成這樣？」

「雖然我們都對能成為威德林大人的妻子感到很開心，但伊娜大人只要能和威德林大人比試魔法就滿足了吧？所以，您沒有勉強自己成為威德林大人妻子的必要呢」

「沒有那種事！我也不想和贏不了自己的男人結婚啊！」

「（真是麻煩的女人誒……）」

如果男人贏不了自己就不和他結婚，你這是在表演艾爾平時很喜歡讀的小說嗎。

「總而言之，來一決勝負吧！想娶我當妻子的話，就得先打倒我」

「我無所謂啊，人沒必要這麼勉強自己娶誰吧？」

「喂！我不是都允許你和我比試了？快點老實比試然後娶我為妻啦！」

「唔———嗯。伊娜大人其實是個傲嬌？」

「出現了呢！謎之鮑邁斯特邊境伯家用語！」

雖然不清楚詳情，但這個詞好像最早是初代大人想出來的。

畢竟初代大人是個不僅僅在魔法，還在文化、藝術、飲食、魔導技術的進步上也做出了諸多貢獻的，很厲害的人。

「總而言之快來比啦！」

「真拿你沒辦法……」

先和她比一次的話，伊娜大人就能暫時老實下來了吧。

比起做這種事，我今天還預定要和大家一起去魔之森狩獵呢，所以得快點弄完。

「今天，我帶來了祖先大人傳下來的強力王牌哦！爸爸已經把這個送給我當陪嫁道具了。這是不僅可以自如在槍尖纏上各種屬性的魔法，甚至還能把那些魔法射出去的，傳說中的魔槍『飛穗丸』！是初代鮑邁斯特邊境伯送給女兒的陪嫁嫁妝呢」

的確，初代大人的女兒中有一人嫁給了王家。

那位女兒的母親好像是槍術高手，所以她的女兒也同樣學習了槍法嗎。

「其實我會槍法哦」

「唔———嗯，輸掉的時間應該能比上次延後二十秒左右吧」

「你真失禮誒。是叫露易絲來著？」

「因為我們都經歷過嘛。對吧，各位」

聽到露易絲這個問題的其他婚約者們全都點了點頭。

「我是治癒魔法專精沒法戰鬥」

「要是使用魔導武器就能贏的話，誰都不必辛苦了」

「畢竟魔力量的差距大到絕望地步呀」

「就是這麼回事了……。伊娜大人，請注意不要受傷」

「威德林，夫婦之間第一次打交道是最重要的，你可別一不小心輸掉導致永遠在她面前抬不起頭啊」

「我覺得，老公就算大意也不會輸的……」

「你們幾個還真敢說啊」

伊娜公主對埃莉絲、維爾瑪、卡特莉娜、卡琪婭、泰蕾莎、麗莎的話發出抱怨。

「算吧。反正實際一動手你就能明白了」

那之後，我立刻和新婚約者伊娜公主進行了魔法比試。

話說回來，我居然會僅僅因為有很多魔力這個理由就增加婚約者數量啊。

根據留下的記錄，初代大人妻子多的驚人，他也和我一樣遭遇過這種情況嗎？

就算比別人擅長魔法些，我也不可能擺脫各種貴族規矩的束縛。不過這方面的問題，我覺得今後總會有辦法解決。

所以最後，我一定會和初代大人一樣，度過很辛苦的人生吧。
