「呼」

在借薩妮婭和伊莉一把手，總算是想辦法讓米蘭達睡在椅子上之後，麻里子嘆了口氣。上下皆身著著水色睡衣的米蘭達，如今正在由椅子組成的臨時床舖上發出著平穩的呼吸聲。有時，會輕輕地搖擺著尾尖，或是無意間發出好似笑聲一樣的微微聲音。

「雖然只一起度過了一天，不過大體上已經知道了米蘭達桑是屬於精力旺盛的那種人，她平時晚上總是這樣的嗎？」

在米蘭達枕邊附近的椅子上坐下的麻里子，拿起放在旁邊才喝了一半的啤酒杯向旁邊打聽著。薩妮婭與伊莉也各自拿著自己的飲品來到了麻里子的桌邊。

「沒有那種事哦。米蘭達大醉的樣子還是第一次見到過」
「平時從來不會喝第二杯，喝成這個樣子我也是一次看見」

身著淡綠色的柔軟睡衣──並非是透明的──並披著披肩的薩妮婭那樣回答，而另一邊則是穿著為了回家而準備的私服──緋色的質樸連衣裙──的伊莉，在面前那稍顯粗胖的酒瓶前放下了自己的酒杯，那樣附和道

「那個？　是蒸餾酒麼？」
「嗯。威士忌酒。要喝？」

伊莉對麻里子的問題點著頭，舉起了那酒瓶。

「可以嗎？」

一邊反問，麻里子一邊看向薩妮婭的臉。本來就不算是很討厭。可雖然對味道感興趣，但如今身無分文的麻里子實在無法去自掏腰包來支付。

「沒關係。這個酒是我的」
「誒，是那樣嗎？」

回答，意外的從伊莉那邊傳了回來。麻里子也看到薩妮婭正微笑的點著頭。

「如今會在這個旅館喝這個的，大概就只有我和老板娘了。因此就預先將所有酒都買下來放在這裡」
「啊啊，原來如此。那麼，就一點點」

（是像ボトルキープ那樣的東西啊）

麻里子稍微看了下四周，拿起米蘭達的空酒杯遞到伊莉面前。在旁邊看著的薩妮婭瞪大了眼睛，不過麻里子並沒有注意到。

「嗯」

伊莉的臉略微的漲紅，啵地一聲拔掉了酒瓶的瓶塞，向麻里子酒杯中倒起酒。淺褐色的瀑布咕嚕咕嚕地傾流而下。

「我開動了」

麻里子舉起啤酒杯，向口中倒入威士忌酒，含在口中。薩妮婭在旁邊一臉想要說什麼的表情，不過麻里子依舊沒有注意到。

在感受酒在嘴中緩緩翻滾之後，麻里子咽了下去。烤焦的酒桶香味，是屬於比較質樸的味道，以一種稍顯粗暴的口感通過了喉嚨。

（噢噢，未成熟的麥芽酒，說的就是這種感覺吧。真不愧是一口氣喝掉就快醉了的啊。米蘭達桑到底喝了多少這個呢）

「麻里子桑？」

麻里子二口，三口的仔細品味著，在一旁的薩妮婭很擔心的呼喚起來。而另一邊的伊莉則是饒有興致的看著麻里子。

「哎？　啊啊，一直不說話很對不起。稍微有點未熟的感覺，不過很好喝」
「不，不是那樣的。麻里子桑，完全不在乎麼？」
「誒？」
「那樣烈的酒，就那樣喝真的不要緊嗎」
「是的，那不要緊⋯⋯啊」

麻里子注意到自己失言了。剛剛已經從伊莉那裡聽說過，威士忌酒黨本身在這裡就是屬於少數派。那麼，還能這樣直接喝的女孩子又究竟會有多少呢。

「誒都，這是那個，對吧⋯⋯」
「也就是說，愛喝酒的伙伴人數增加了」

就在麻里子想著該找些什麼藉口的時候，伊莉像是要堵住那藉口一樣插話進來。

「誒，啊啊，那樣啊。麻里子桑，要是喜歡的話就直說嘛」
「嗯」
「誒誒！？　不對不對，請等一下」

（那確實是不討厭的，但雖說不討厭⋯⋯）

「嗯，嗯姆⋯⋯」

正當麻里子打算說出更多話的時候，躺在臨時床舖上的米蘭達很不舒服的呻吟著轉了個個。

「啊，米蘭達，你沒問題吧？」
「嗯姆，噝」

對於詢問著的薩妮婭，基本就像是夢話一樣的回答，米蘭達又睡過去了。

「那個啤酒杯的三分之一」

注視著米蘭達的睡臉，伊莉一個人獨自說道。

「誒？」
「是米蘭達喝的量」

雖說是小啤酒杯，不過三分之一也有一百毫升了。直接那麼快速喝下去也是足夠醉掉的量。在那之前，啤酒應該也是喝的太多了。

「這個不起來啊。不好意思，能領我到房間去嗎？」
「我明白了」
「嗯」
「總之，我先把她抱起來，伊莉桑請從另一邊幫我下」
「知道了」

麻里子在椅子床的旁邊彎下腰，將手伸到米蘭達的身下。

「米蘭達桑，如果能抱的話請抓住我」
「嗯～？　嗯」

米蘭達抖動著耳朵含糊的回答了麻里子的聲音，閉著眼睛抬起手臂緊緊摟住了麻里子的脖子。

「哇」

一瞬間感到略微吃驚的麻里子，就那樣用手抬在米蘭達後背和膝蓋的下方慢慢地站了起來。即所謂的公主抱形式。站起來意外的比麻里子想像中還要輕鬆。

（好，被抱上來的話相當輕鬆吶。是說米蘭達桑很輕嗎。不，這是麻里子肌肉力量太高了吧？）

「厲害的」
「麻里子桑，那個沒問題麼？」
「差不多吧。我就這樣過去，請拜託一起陪我去房間那」

回答了擔心著的薩妮婭，麻里子向旅館深處調轉了方向。

「明白了。當心腳下吶」

奇怪的隊伍開始移動了。